package com.is425;

import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.*;

public class VictimSummaryPeriod {
    
    public int year;
    public int periodNumber;
    public int numCrimes;

    // ABSOLUTELY POSITIVELY NEED AN EMPTY CONSTRUCTOR!!!
    VictimSummaryPeriod() {}

    VictimSummaryPeriod(int year, int periodNumber) {
        this.year = year;
        this.periodNumber = periodNumber;
        this.numCrimes = 0;
    }

    VictimSummaryPeriod(int year, int periodNumber, int numCrimes) {
        this.year = year;
        this.periodNumber = periodNumber;
        this.numCrimes = numCrimes;
    }
}

package com.is425;

public class PeriodPrediction {
    public String periodType;
    public int periodNumber;
    public int mean;
    public int stdDev;
    public int expectedMin;
    public int expectedMax;
    public String name;

    public PeriodPrediction() {}

    public String getPeriodType() {
        return this.periodType;
    }

    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public int getPeriodNumber() {
        return this.periodNumber;
    }

    public void setPeriodNumber(int periodNumber) {
        this.periodNumber = periodNumber;
    }

    public int getMean() {
        return this.mean;
    }

    public void setMean(int mean) {
        this.mean = mean;
    }

    public int getStdDev() {
        return this.stdDev;
    }

    public void setStdDev(int stdDev) {
        this.stdDev = stdDev;
    }

    public int getExpectedMin() {
        return this.expectedMin;
    }

    public void setExpectedMin(int expectedMin) {
        this.expectedMin = expectedMin;
    }

    public int getExpectedMax() {
        return this.expectedMax;
    }

    public void setExpectedMax(int expectedMax) {
        this.expectedMax = expectedMax;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

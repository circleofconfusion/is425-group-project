package com.is425;

import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.*;

public class VictimSummaryCrime {
   
    // the type of period we're working with: month, week
    public String periodType;
    // the name of the type of crime we're examining
    public String crime;
    // The total number of crimes of this type for all data
    public int totalCrimes;
    // The highest number of crimes to happen in all of the periods
    public int periodMax;
    // a list of crime summaries by period for this type of crime
    public List<VictimSummaryPeriod> periodData; 

    // ABSOLUTELY POSITIVELY NEED AN EMPTY CONSTRUCTOR!!!
    VictimSummaryCrime() {
        periodData = new ArrayList<>();
    }

    VictimSummaryCrime(String crime, String periodType) {
        this();
        this.crime = crime;
        this.periodType = periodType;
        this.totalCrimes = 0;
        this.periodMax = 0;
    }
}

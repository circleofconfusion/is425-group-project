package com.is425;

import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.*;

@XmlRootElement
public class VictimSummaryLocation {
   
    // the name of the location 
    public String location;
    // the type of location (neighborhood, district)
    public String locationType;
    // List of crime summary data by period.
    public List<VictimSummaryCrime> crimeData;

    // ABSOLUTELY POSITIVELY NEED AN EMPTY CONSTRUCTOR!!!
    public VictimSummaryLocation() {
        this.crimeData = new ArrayList<>();
    }

    public VictimSummaryLocation(String location, String locationType) {
        this();
        this.location = location;
        this.locationType = locationType;
    }
}

package com.is425;

public class VictimPoint {

    public int totalCrimes;
    public int violentCrimes;
    public int nonviolentCrimes;
    public double latitude;
    public double longitude;

    public VictimPoint() {}

    public int getTotalCrimes() {
        return this.totalCrimes;
    }

    public void setTotalCrimes(int totalCrimes) {
        this.totalCrimes = totalCrimes;
    }

    public int getViolentCrimes() {
        return this.violentCrimes;
    }

    public void setViolentCrimes(int violentCrimes) {
        this.violentCrimes = violentCrimes;
    }

    public int getNonviolentCrimes() {
        return this.nonviolentCrimes;
    }

    public void setNonviolentCrimes(int nonviolentCrimes) {
        this.nonviolentCrimes = nonviolentCrimes;
    }
    
    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}

package com.is425;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.InputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Date;
import java.util.Properties;
import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import javax.servlet.ServletContext;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("/victimService")
public class VictimService {

    // logging setup
    private static final Logger logger = LogManager.getLogger(VictimService.class);
    
    /**
     * 
     * @return List<VictimRecord> in JSON
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8;")
    public List<VictimRecord> read(@QueryParam("neighborhood") String neighborhood,
        @QueryParam("district") String district) {
        List<VictimRecord> vrList = new ArrayList<>();
        DataSource ds = getDataSource();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;

        try {
            con = ds.getConnection();
            
            if (neighborhood != null && district != null) {
                ps = con.prepareStatement("SELECT * FROM victimReport WHERE neighborhood=? AND district=?");
                ps.setString(1,neighborhood);
                ps.setString(2,district);
            }
            else if (neighborhood != null) {           
                ps = con.prepareStatement("SELECT * FROM victimReport WHERE neighborhood=?");
                ps.setString(1,neighborhood);
            }
            else if (district != null) {
                ps = con.prepareStatement("SELECT * FROM victimReport WHERE district=?");
                ps.setString(1,district);
            }
            else {
                ps = con.prepareStatement("SELECT * FROM victimReport");
            }

            rs = ps.executeQuery();
            
            while (rs.next()) {
                VictimRecord vr = new VictimRecord();

                vr.setId(rs.getLong("id"));
                vr.setDate(rs.getString("date"));
                vr.setTime(rs.getString("time"));
                vr.setCrimeCode(rs.getString("crimecode"));
                vr.setDescription(rs.getString("crime"));
                vr.setWeapon(rs.getString("weapon"));
                vr.setPost(rs.getString("post"));
                vr.setDistrict(rs.getString("district"));
                vr.setNeighborhood(rs.getString("neighborhood"));
                vr.setLatitude(rs.getDouble("latitude"));
                vr.setLongitude(rs.getDouble("longitude"));

                vrList.add(vr);
            }
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        finally {
            try {
                if (ps != null) ps.close();
                if (con != null) con.close();
            }
            catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        return vrList;
    }

    @GET
    @Path("/summary/{areaType}/{periodType}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8;")
    public List<VictimSummaryLocation> summaryByLocation(
        @PathParam("areaType") String areaType, 
        @PathParam("periodType") String periodType,
        @DefaultValue("all") @QueryParam("crimeType") String crimeType) {

        List<VictimSummaryLocation> vsList = new ArrayList<>();
        DataSource ds = getDataSource();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        VictimSummaryLocation vsl = null;
        VictimSummaryCrime arson = null;
        VictimSummaryCrime commonAssault = null;
        VictimSummaryCrime aggravatedAssault = null;
        VictimSummaryCrime assaultByThreat = null;
        VictimSummaryCrime burglary = null;
        VictimSummaryCrime autoTheft = null;
        VictimSummaryCrime larcenyFromAuto = null;
        VictimSummaryCrime larceny = null;
        VictimSummaryCrime robberyCarjacking = null;
        VictimSummaryCrime robberyCommercial = null;
        VictimSummaryCrime robberyResidence = null;
        VictimSummaryCrime robberyStreet = null;
        VictimSummaryCrime rape = null;
        VictimSummaryCrime shooting = null;
        VictimSummaryCrime homicide = null;
        int currPeriod = -1; // keeps track of the current period number -1 b/c java can't do null here

        try {
            con = ds.getConnection();
            
            if (areaType.equals("district")) { // TODO get district from neighborhood table
                if (periodType.equals("week")) {
                    ps = con.prepareStatement("SELECT district.district AS location, YEAR(vr.date) AS year, WEEK(vr.date) AS period, ct.crime, COUNT(vr.crime) AS crimecount FROM district, crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) WHERE vr.district != '' AND vr.district=district.id GROUP BY district.district, year, period, vr.crime");
                }
                else if (periodType.equals("month")) {
                    ps = con.prepareStatement("SELECT district.district AS location, YEAR(vr.date) AS year, MONTH(vr.date) AS period, ct.crime, COUNT(vr.crime) AS crimecount FROM district, crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) WHERE vr.district != '' AND vr.district=district.id GROUP BY district.district, year, period, vr.crime");
                }
            }
            else if (areaType.equals("neighborhood")) {
                if (periodType.equals("week")) {
                    ps = con.prepareStatement("SELECT neighborhood.neighborhood AS location, YEAR(vr.date) AS year, WEEK(vr.date) AS period, ct.crime, COUNT(vr.crime) AS crimecount FROM neighborhood, crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) WHERE vr.neighborhood != '' AND vr.neighborhood=neighborhood.id GROUP BY neighborhood.neighborhood, year, period, vr.crime");
                }
                else if (periodType.equals("month")) {
                    ps = con.prepareStatement("SELECT neighborhood.neighborhood AS location, YEAR(vr.date) AS year, MONTH(vr.date) AS period, ct.crime, COUNT(vr.crime) AS crimecount FROM neighborhood, crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) WHERE vr.neighborhood != '' AND vr.neighborhood=neighborhood.id GROUP BY neighborhood.neighborhood, year, period, vr.crime");
                }
            }
            else if (areaType.equals("city")) {
                if (periodType.equals("week")) {
                    ps = con.prepareStatement("SELECT 'Baltimore' AS location, YEAR(vr.date) AS year, WEEK(vr.date) AS period, ct.crime, COUNT(vr.crime) AS crimecount FROM crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) GROUP BY year, period, vr.crime");
                }
                else if (periodType.equals("month")) {
                    ps = con.prepareStatement("SELECT 'Baltimore' AS location, YEAR(vr.date) AS year, MONTH(vr.date) AS period, ct.crime, COUNT(vr.crime) AS crimecount FROM crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) GROUP BY year, period, vr.crime");
                }
            }
               
            rs = ps.executeQuery();
            while(rs.next()) {
                // see if we're still dealing with the same location
                if (vsl == null || !rs.getString("location").trim().equals(vsl.location)) {
                    // save the old VictimLocationSummary
                    if (vsl != null) {
                        // append crime data to location
                        if (crimeType.equals("all") || crimeType.equals("ARSON"))
                            vsl.crimeData.add(arson);
                        if (crimeType.equals("all") || crimeType.equals("COMMON ASSAULT"))
                            vsl.crimeData.add(commonAssault);
                        if (crimeType.equals("all") || crimeType.equals("AGG. ASSAULT"))
                            vsl.crimeData.add(aggravatedAssault);
                        if (crimeType.equals("all") || crimeType.equals("ASSAULT BY THREAT"))
                            vsl.crimeData.add(assaultByThreat);
                        if (crimeType.equals("all") || crimeType.equals("BURGLARY"))
                            vsl.crimeData.add(burglary);
                        if (crimeType.equals("all") || crimeType.equals("AUTO THEFT"))
                            vsl.crimeData.add(autoTheft);
                        if (crimeType.equals("all") || crimeType.equals("LARCENY FROM AUTO"))
                            vsl.crimeData.add(larcenyFromAuto);
                        if (crimeType.equals("all") || crimeType.equals("LARCENY"))
                            vsl.crimeData.add(larceny);
                        if (crimeType.equals("all") || crimeType.equals("ROBBERY - CARJACKING"))
                            vsl.crimeData.add(robberyCarjacking);
                        if (crimeType.equals("all") || crimeType.equals("ROBBERY - COMMERCIAL"))
                            vsl.crimeData.add(robberyCommercial);
                        if (crimeType.equals("all") || crimeType.equals("ROBBERY - RESIDENCE"))
                            vsl.crimeData.add(robberyResidence);
                        if (crimeType.equals("all") || crimeType.equals("ROBBERY - STREET"))
                            vsl.crimeData.add(robberyStreet);
                        if (crimeType.equals("all") || crimeType.equals("RAPE"))
                            vsl.crimeData.add(rape);
                        if (crimeType.equals("all") || crimeType.equals("SHOOTING"))
                            vsl.crimeData.add(shooting);
                        if (crimeType.equals("all") || crimeType.equals("HOMICIDE"))
                            vsl.crimeData.add(homicide);
                        // append location to summary list
                        vsList.add(vsl);
                    }

                    // initialize a new summary location
                    vsl = new VictimSummaryLocation(rs.getString("location"), areaType);

                    // initialize the new location's crime listings
                    arson = new VictimSummaryCrime("Arson", periodType);
                    commonAssault = new VictimSummaryCrime("Common Assault", periodType);
                    aggravatedAssault = new VictimSummaryCrime("Aggravated Assault", periodType);
                    assaultByThreat = new VictimSummaryCrime("Assault by Threat", periodType);
                    burglary = new VictimSummaryCrime("Burglary", periodType);
                    autoTheft = new VictimSummaryCrime("Auto Theft", periodType);
                    larcenyFromAuto = new VictimSummaryCrime("Larceny from Auto", periodType);
                    larceny = new VictimSummaryCrime("Larceny", periodType);
                    robberyCarjacking = new VictimSummaryCrime("Robbery - Carjacking", periodType);
                    robberyCommercial = new VictimSummaryCrime("Robbery - Commercial", periodType);
                    robberyResidence = new VictimSummaryCrime("Robbery - Residence", periodType);
                    robberyStreet = new VictimSummaryCrime("Robbery - Street", periodType);
                    rape = new VictimSummaryCrime("Rape", periodType);
                    shooting = new VictimSummaryCrime("Shooting", periodType);
                    homicide = new VictimSummaryCrime("Homicide", periodType);
                }

                // on changeover of period number, push a new 0-value VictimSummaryPeriod for each crime
                if (rs.getInt("period") != currPeriod) {
                    arson.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    commonAssault.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    aggravatedAssault.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    assaultByThreat.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    burglary.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    autoTheft.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    larcenyFromAuto.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    larceny.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    robberyCarjacking.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    robberyCommercial.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    robberyResidence.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    robberyStreet.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    rape.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    shooting.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    homicide.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    
                    // now that we're all initialized, set currPeriod to this period
                    currPeriod = rs.getInt("period");
                }

                if (rs.getString("crime").equals("ARSON")) {
                    int crimeCount = rs.getInt("crimeCount");
                    arson.totalCrimes += crimeCount;
                    if (arson.periodMax < crimeCount) arson.periodMax = crimeCount;
                    arson.periodData.get(arson.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("COMMON ASSAULT")) {
                    int crimeCount = rs.getInt("crimeCount");
                    commonAssault.totalCrimes += crimeCount;
                    if (commonAssault.periodMax < crimeCount) commonAssault.periodMax = crimeCount;
                    commonAssault.periodData.get(commonAssault.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("AGG. ASSAULT")) {
                    int crimeCount = rs.getInt("crimeCount");
                    aggravatedAssault.totalCrimes += crimeCount;
                    if (aggravatedAssault.periodMax < crimeCount) aggravatedAssault.periodMax = crimeCount;
                    aggravatedAssault.periodData.get(aggravatedAssault.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("ASSAULT BY THREAT")) {
                    int crimeCount = rs.getInt("crimeCount");
                    assaultByThreat.totalCrimes += crimeCount;
                    if (assaultByThreat.periodMax < crimeCount) assaultByThreat.periodMax = crimeCount;
                    assaultByThreat.periodData.get(assaultByThreat.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("BURGLARY")) {
                    int crimeCount = rs.getInt("crimeCount");
                    burglary.totalCrimes += crimeCount;
                    if (burglary.periodMax < crimeCount) burglary.periodMax = crimeCount;
                    burglary.periodData.get(burglary.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("AUTO THEFT")) {
                    int crimeCount = rs.getInt("crimeCount");
                    autoTheft.totalCrimes += crimeCount;
                    if (autoTheft.periodMax < crimeCount) autoTheft.periodMax = crimeCount;
                    autoTheft.periodData.get(autoTheft.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("LARCENY FROM AUTO")) {
                    int crimeCount = rs.getInt("crimeCount");
                    larcenyFromAuto.totalCrimes += crimeCount;
                    if (larcenyFromAuto.periodMax < crimeCount) larcenyFromAuto.periodMax = crimeCount;
                    larcenyFromAuto.periodData.get(larcenyFromAuto.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("LARCENY")) {
                    int crimeCount = rs.getInt("crimeCount");
                    larceny.totalCrimes += crimeCount;
                    if (larceny.periodMax < crimeCount) larceny.periodMax = crimeCount;
                    larceny.periodData.get(larceny.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("ROBBERY - CARJACKING")) {
                    int crimeCount = rs.getInt("crimeCount");
                    robberyCarjacking.totalCrimes += crimeCount;
                    if (robberyCarjacking.periodMax < crimeCount) robberyCarjacking.periodMax = crimeCount;
                    robberyCarjacking.periodData.get(robberyCarjacking.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("ROBBERY - COMMERCIAL")) {
                    int crimeCount = rs.getInt("crimeCount");
                    robberyCommercial.totalCrimes += crimeCount;
                    if (robberyCommercial.periodMax < crimeCount) robberyCommercial.periodMax = crimeCount;
                    robberyCommercial.periodData.get(robberyCommercial.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("ROBBERY - RESIDENCE")) {
                    int crimeCount = rs.getInt("crimeCount");
                    robberyResidence.totalCrimes += crimeCount;
                    if (robberyResidence.periodMax < crimeCount) robberyResidence.periodMax = crimeCount;
                    robberyResidence.periodData.get(robberyResidence.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("ROBBERY - STREET")) {
                    int crimeCount = rs.getInt("crimeCount");
                    robberyStreet.totalCrimes += crimeCount;
                    if (robberyStreet.periodMax < crimeCount) robberyStreet.periodMax = crimeCount;
                    robberyStreet.periodData.get(robberyStreet.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("RAPE")) {
                    int crimeCount = rs.getInt("crimeCount");
                    rape.totalCrimes += crimeCount;
                    if (rape.periodMax < crimeCount) rape.periodMax = crimeCount;
                    rape.periodData.get(rape.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("SHOOTING")) {
                    int crimeCount = rs.getInt("crimeCount");
                    shooting.totalCrimes += crimeCount;
                    if (shooting.periodMax < crimeCount) shooting.periodMax = crimeCount;
                    shooting.periodData.get(shooting.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (rs.getString("crime").equals("HOMICIDE")) {
                    int crimeCount = rs.getInt("crimeCount");
                    homicide.totalCrimes += crimeCount;
                    if (homicide.periodMax < crimeCount) homicide.periodMax = crimeCount;
                    homicide.periodData.get(homicide.periodData.size() - 1).numCrimes = crimeCount;
                }
            } // end while

            // since this is the last time, commit the last vsp and vsl
            // append crime data to location
            if (crimeType.equals("all") || crimeType.equals("ARSON"))
                vsl.crimeData.add(arson);
            if (crimeType.equals("all") || crimeType.equals("COMMON ASSAULT"))
                vsl.crimeData.add(commonAssault);
            if (crimeType.equals("all") || crimeType.equals("AGG. ASSAULT"))
                vsl.crimeData.add(aggravatedAssault);
            if (crimeType.equals("all") || crimeType.equals("ASSAULT BY THREAT"))
                vsl.crimeData.add(assaultByThreat);
            if (crimeType.equals("all") || crimeType.equals("BURGLARY"))
                vsl.crimeData.add(burglary);
            if (crimeType.equals("all") || crimeType.equals("AUTO THEFT"))
                vsl.crimeData.add(autoTheft);
            if (crimeType.equals("all") || crimeType.equals("LARCENY FROM AUTO"))
                vsl.crimeData.add(larcenyFromAuto);
            if (crimeType.equals("all") || crimeType.equals("LARCENY"))
                vsl.crimeData.add(larceny);
            if (crimeType.equals("all") || crimeType.equals("ROBBERY - CARJACKING"))
                vsl.crimeData.add(robberyCarjacking);
            if (crimeType.equals("all") || crimeType.equals("ROBBERY - COMMERCIAL"))
                vsl.crimeData.add(robberyCommercial);
            if (crimeType.equals("all") || crimeType.equals("ROBBERY - RESIDENCE"))
                vsl.crimeData.add(robberyResidence);
            if (crimeType.equals("all") || crimeType.equals("ROBBERY - STREET"))
                vsl.crimeData.add(robberyStreet);
            if (crimeType.equals("all") || crimeType.equals("RAPE"))
                vsl.crimeData.add(rape);
            if (crimeType.equals("all") || crimeType.equals("SHOOTING"))
                vsl.crimeData.add(shooting);
            if (crimeType.equals("all") || crimeType.equals("HOMICIDE"))
                vsl.crimeData.add(homicide);

            // append location to summary list
            vsList.add(vsl);
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        finally {
            try {
                if (ps != null) ps.close();
                if (con != null) con.close();
            }
            catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        
        return vsList;
    }

    /**
     * Provides violent/nonviolent crime amounts over time.
     */
    @GET
    @Path("/summary/violent-nonviolent/{areaType}/{periodType}/{crimeType}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8;")
    public List<VictimSummaryLocation> summaryByViolentNonviolent(
        @PathParam("areaType") String areaType, 
        @PathParam("periodType") String periodType,
        @PathParam("crimeType") String crimeType) {

        List<VictimSummaryLocation> vsList = new ArrayList<>();
        DataSource ds = getDataSource();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        VictimSummaryLocation vsl = null;
        VictimSummaryCrime violent = null;
        VictimSummaryCrime nonviolent = null;
        int currPeriod = -1; // keeps track of the current period number -1 b/c java can't do null here

        try {
            con = ds.getConnection();
            
            if (crimeType.equals("nonviolent")) { 
                 if (areaType.equals("district")) {
                    ps = con.prepareStatement("SELECT district.district AS location, " + 
                        "YEAR(vr.date) AS year, MONTH(vr.date) AS period, " +
                        "IF(ct.violent = 1, 'violent', 'nonviolent') as type, vr.crime, COUNT(vr.crime) AS crimeCount " +
                        "FROM district, crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) " +
                        "WHERE vr.district != '' AND vr.district=district.id AND ct.violent = 0 " + 
                        "GROUP BY district.district, year, period, ct.violent");
                }
                else if (areaType.equals("city")) {
                    // need the vr.crime field for some reason...
                    // See: http://stackoverflow.com/questions/19062453/java-sq-sqlexception-column-not-found
                    ps = con.prepareStatement("SELECT 'Baltimore' AS location, " + 
                        "YEAR(vr.date) AS year, MONTH(vr.date) AS period, " +
                        "IF(ct.violent = 1, 'violent', 'nonviolent') as type, COUNT(vr.crime) AS crimeCount " +
                        "FROM crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) " +
                        "WHERE ct.violent = 0 "+
                        "GROUP BY year, period, ct.violent");
                }
            }
            else if (crimeType.equals("violent")) {
                 if (areaType.equals("district")) {
                    ps = con.prepareStatement("SELECT district.district AS location, " + 
                        "YEAR(vr.date) AS year, MONTH(vr.date) AS period, " +
                        "IF(ct.violent = 1, 'violent', 'nonviolent') as type, vr.crime, COUNT(vr.crime) AS crimeCount " +
                        "FROM district, crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) " +
                        "WHERE vr.district != '' AND vr.district=district.id AND ct.violent = 1 " + 
                        "GROUP BY district.district, year, period, ct.violent");
                }
                else if (areaType.equals("city")) {
                    // need the vr.crime field for some reason...
                    // See: http://stackoverflow.com/questions/19062453/java-sq-sqlexception-column-not-found
                    ps = con.prepareStatement("SELECT 'Baltimore' AS location, " + 
                        "YEAR(vr.date) AS year, MONTH(vr.date) AS period, " +
                        "IF(ct.violent = 1, 'violent', 'nonviolent') as type, COUNT(vr.crime) AS crimeCount " +
                        "FROM crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) " +
                        "WHERE ct.violent = 1 "+
                        "GROUP BY year, period, ct.violent");
                }
            }
            else { // both
                if (areaType.equals("district")) {
                    ps = con.prepareStatement("SELECT district.district AS location, " + 
                        "YEAR(vr.date) AS year, MONTH(vr.date) AS period, " +
                        "IF(ct.violent = 1, 'violent', 'nonviolent') as type, vr.crime, COUNT(vr.crime) AS crimeCount " +
                        "FROM district, crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) " +
                        "WHERE vr.district != '' AND vr.district=district.id " + 
                        "GROUP BY district.district, year, period, ct.violent");
                }
                else if (areaType.equals("city")) {
                    // need the vr.crime field for some reason...
                    // See: http://stackoverflow.com/questions/19062453/java-sq-sqlexception-column-not-found
                    ps = con.prepareStatement("SELECT 'Baltimore' AS location, " + 
                        "YEAR(vr.date) AS year, MONTH(vr.date) AS period, " +
                        "IF(ct.violent = 1, 'violent', 'nonviolent') as type, COUNT(vr.crime) AS crimeCount " +
                        "FROM crimeType ct LEFT JOIN victimReport vr ON (ct.id=vr.crime) " +
                        "GROUP BY year, period, ct.violent");
                }
            }

            rs = ps.executeQuery();
            while(rs.next()) { 
                // see if we're still dealing with the same location
                if (vsl == null || !rs.getString("location").trim().equals(vsl.location)) {
                    // save the old VictimLocationSummary
                    if (vsl != null) {
                        // append crime data to location
                        vsl.crimeData.add(violent);
                        vsl.crimeData.add(nonviolent);
                        
                        // append location to summary list
                        vsList.add(vsl);
                    }

                    // initialize a new summary location
                    vsl = new VictimSummaryLocation(rs.getString("location"), areaType);

                    // initialize the new location's crime listings
                    violent = new VictimSummaryCrime("Violent", periodType);
                    nonviolent = new VictimSummaryCrime("Nonviolent", periodType);
                }

                // on changeover of period number, push a new 0-value VictimSummaryPeriod for each crime
                if (rs.getInt("period") != currPeriod) {
                    violent.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
                    nonviolent.periodData.add(new VictimSummaryPeriod(rs.getInt("year"), rs.getInt("period")));
  
                    // now that we're all initialized, set currPeriod to this period
                    currPeriod = rs.getInt("period");
                }

                if (crimeType.equals("violent") || crimeType.equals("both") && rs.getString("type").equals("violent")) {
                    int crimeCount = rs.getInt("crimeCount");
                    violent.totalCrimes += crimeCount;
                    if (violent.periodMax < crimeCount) violent.periodMax = crimeCount;
                    violent.periodData.get(violent.periodData.size() - 1).numCrimes = crimeCount;
                }
                else if (crimeType.equals("nonviolent") || crimeType.equals("both") && rs.getString("type").equals("nonviolent")) {
                    int crimeCount = rs.getInt("crimeCount");
                    nonviolent.totalCrimes += crimeCount;
                    if (nonviolent.periodMax < crimeCount) nonviolent.periodMax = crimeCount;
                    nonviolent.periodData.get(nonviolent.periodData.size() - 1).numCrimes = crimeCount;
                }
            
            } // end while

            // since this is the last time, commit the last vsp and vsl
            // append crime data to location
            if (crimeType.equals("violent") || crimeType.equals("both"))
                vsl.crimeData.add(violent);
            if (crimeType.equals("nonviolent") || crimeType.equals("both"))
                vsl.crimeData.add(nonviolent);

            vsList.add(vsl);
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        finally {
            try {
                if (ps != null) ps.close();
                if (con != null) con.close();
            }
            catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return vsList;
    }

    /**
     * Provides location and frequency of victim reports.
     */
    @GET
    @Path("/summary/crimelocation")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8;")
    public List<VictimPoint> victimPointLocations(
        @DefaultValue("all") @QueryParam("district") String district) {

        List<VictimPoint> vpl = new ArrayList<>();
        DataSource ds = getDataSource();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = ds.getConnection();

            if (district.equals("all")) {
                ps = con.prepareStatement("SELECT victimReport.latitude, victimReport.longitude, count(*) AS totalCrimes, SUM(CASE WHEN crimeType.violent = 0 THEN 1 ELSE 0 END) AS violentCrimes, SUM(CASE WHEN crimeType.violent = 0 THEN 0 ELSE 1 END) AS nonviolentCrimes FROM victimReport, crimeType WHERE victimReport.crime = crimeType.id AND latitude != 0 AND longitude != 0 GROUP BY latitude, longitude");
            }
            else {
                ps = con.prepareStatement("SELECT victimReport.latitude, victimReport.longitude, count(*) AS totalCrimes, SUM(CASE WHEN crimeType.violent = 0 THEN 1 ELSE 0 END) AS violentCrimes, SUM(CASE WHEN crimeType.violent = 0 THEN 0 ELSE 1 END) AS nonviolentCrimes FROM victimReport, crimeType, district WHERE victimReport.crime = crimeType.id AND victimReport.district = district.id AND latitude != 0 AND longitude != 0 AND district.district=? GROUP BY latitude, longitude");
                ps.setString(1, district);
            }
            rs = ps.executeQuery();
            while(rs.next()) {
                VictimPoint vp = new VictimPoint();
                vp.totalCrimes = rs.getInt("totalCrimes");
                vp.violentCrimes = rs.getInt("violentCrimes");
                vp.nonviolentCrimes = rs.getInt("nonviolentCrimes");
                vp.latitude = rs.getDouble("latitude");
                vp.longitude = rs.getDouble("longitude");
                vpl.add(vp);
            }
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        finally {
            try {
                if (ps != null) ps.close();
                if (con != null) con.close();
            }
            catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return vpl;
    }

    /**
     * Provides details about a single location.
     * @param longitude The longitude of the location.
     * @param latitude  The latitude of the location.
     */
    @GET
    @Path("/crimelocation/detail")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8;")
    public List<VictimLocationDetail> getVictimLocationDetail(
        @QueryParam("latitude") double latitude,
        @QueryParam("longitude") double longitude
        ) {

        List<VictimLocationDetail> vldl = new ArrayList<>();
        DataSource ds = getDataSource();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = ds.getConnection();

            ps = con.prepareStatement("SELECT date, time, location, crimeType.crime AS crime, crimeType.violent, weapon, " +
                    "district.district AS district, neighborhood.neighborhood as neighborhood " +
                    "FROM victimReport, crimeType, district, neighborhood " +
                    "WHERE victimReport.crime = crimeType.id AND victimReport.district = district.id " +
                    "AND victimReport.neighborhood = neighborhood.id AND latitude = ? AND longitude = ?");
            ps.setDouble(1, latitude);
            ps.setDouble(2, longitude);

            rs = ps.executeQuery();
            while(rs.next()) {
                VictimLocationDetail vld = new VictimLocationDetail();
                vld.date = rs.getString("date");
                vld.time = rs.getString("time");
                vld.location = rs.getString("location");
                vld.crime = rs.getString("crime");
                vld.weapon = rs.getString("weapon");
                int violent = rs.getInt("violent");
                if(violent == 1)
                    vld.violent = true;
                else
                    vld.violent = false;
                vld.district = rs.getString("district");
                vld.neighborhood = rs.getString("neighborhood");

                vldl.add(vld);
            }
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        finally {
            try {
                if (ps != null) ps.close();
                if (con != null) con.close();
            }
            catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return vldl;

    }

    /**
     * Provides details about a single location.
     * @param longitude The longitude of the location.
     * @param latitude  The latitude of the location.
     */
    @GET
    @Path("/periodPrediction/{locationType}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8;")
    public List<PeriodPrediction> getPeriodPrediction(
        @PathParam("locationType") String locationType,
        @DefaultValue("all") @QueryParam("name") String name) {

        List<PeriodPrediction> ppl = new ArrayList<>();
        DataSource ds = getDataSource();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = ds.getConnection();

            // get prediction for this week
            if (locationType.equals("district")) {
                ps = con.prepareStatement("SELECT history.week, ROUND(AVG(history.count)) AS mean, " +
                    "ROUND(STD(history.count)) as stdDev " + 
                    "FROM (SELECT WEEK(date) AS week, YEAR(date) AS year, COUNT(*) AS count FROM victimReport, district " +
                    "WHERE district.district = ? AND victimReport.district = district.id AND YEAR(date) < YEAR(NOW()) AND WEEK(date) = WEEK(NOW()) GROUP BY year) history");
                ps.setString(1, name);
            }
            else if (locationType.equals("city")) {
                ps = con.prepareStatement("SELECT history.week, ROUND(AVG(history.count)) AS mean, " +
                    "ROUND(STD(history.count)) as stdDev " + 
                    "FROM (SELECT WEEK(date) AS week, YEAR(date) AS year, COUNT(*) AS count FROM victimReport " +
                    "WHERE YEAR(date) < YEAR(NOW()) AND WEEK(date) = WEEK(NOW()) GROUP BY year) history");
            }

            rs = ps.executeQuery();
            PeriodPrediction ppw = new PeriodPrediction();
            while(rs.next()) {
                ppw.periodType = "week";
                ppw.periodNumber = rs.getInt("week");
                ppw.mean = rs.getInt("mean");
                ppw.stdDev = rs.getInt("stdDev");
                ppw.expectedMin = ppw.mean - 2 * ppw.stdDev;
                ppw.expectedMax = ppw.mean + 2 * ppw.stdDev;
                if (locationType.equals("district"))
                    ppw.name = name;
                else
                    ppw.name = "Baltimore City";
            }

            ppl.add(ppw);

            // get prediction for this month
            if (locationType.equals("district")) {
                ps = con.prepareStatement("SELECT history.month, ROUND(AVG(history.count)) AS mean, " +
                    "ROUND(STD(history.count)) as stdDev " + 
                    "FROM (SELECT MONTH(date) AS month, YEAR(date) AS year, COUNT(*) AS count FROM victimReport, district " +
                    "WHERE district.district = ? AND victimReport.district = district.id AND YEAR(date) < YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) GROUP BY year) history");
                ps.setString(1, name);
            }
            else if (locationType.equals("city")) {
                ps = con.prepareStatement("SELECT history.month, ROUND(AVG(history.count)) AS mean, " +
                    "ROUND(STD(history.count)) as stdDev " + 
                    "FROM (SELECT MONTH(date) AS month, YEAR(date) AS year, COUNT(*) AS count FROM victimReport " +
                    "WHERE YEAR(date) < YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) GROUP BY year) history");
            }

            rs = ps.executeQuery();
            PeriodPrediction ppm = new PeriodPrediction();
            while(rs.next()) {
                ppm.periodType = "month";
                ppm.periodNumber = rs.getInt("month");
                ppm.mean = rs.getInt("mean");
                ppm.stdDev = rs.getInt("stdDev");
                ppm.expectedMin = ppm.mean - 2 * ppm.stdDev;
                ppm.expectedMax = ppm.mean + 2 * ppm.stdDev;
                if (locationType.equals("district"))
                    ppm.name = name;
                else
                    ppm.name = "Baltimore City";
            }

            ppl.add(ppm);

        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        finally {
            try {
                if (ps != null) ps.close();
                if (con != null) con.close();
            }
            catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return ppl;
    }
    
    public DataSource getDataSource() {
        Properties p = new Properties();
        InputStream fis = null;
        MysqlDataSource ds = null;

        try {
            fis = Thread.currentThread().getContextClassLoader().getResourceAsStream("db.properties");
            p.load(fis);
            ds = new MysqlDataSource();
            ds.setURL(p.getProperty("MYSQL_DB_URL"));
            ds.setUser(p.getProperty("MYSQL_DB_USERNAME"));
            ds.setPassword(p.getProperty("MYSQL_DB_PASSWORD"));
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }

        return ds;
    }
}

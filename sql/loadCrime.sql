LOAD DATA LOCAL INFILE "neighborhoods.csv"
INTO TABLE neighborhood
FIELDS TERMINATED BY ','
IGNORE 1 LINES
(neighborhood,
    population, white, african_american, native_american, asian,
    pacific_islander, other_race, two_or_more_race, hispanic,
    male, female,
    age_0_4, age_5_11, age_12_14, age_15_17, age_18_24, age_24_34,
    age_35_44, age_45_64, age_over_65,
    families, married, married_18, male_head_household, male_head_household_18,
    female_head_household, female_head_household_18,
    housing, occupied, occupied_by_owner, occupied_by_renter, vacant,
    vacant_rent, vacant_sale, vacant_other,
    @_district, area, c_lat, c_lon)
SET id=NULL,
    district=(SELECT id FROM district WHERE district=@_district);

LOAD DATA LOCAL INFILE "CrimePart1.csv"
INTO TABLE victimReport
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
IGNORE 1 LINES
(@_date,@_time,crimeCode,location,@_desc,weapon,post,@_district,@_neighborhood,@_location1)
SET id=NULL,
    date=STR_TO_DATE(@_date,'%m/%d/%Y'),
    time=STR_TO_DATE(@_time,'%H:%i:%s'),
    crime=(SELECT id FROM crimeType WHERE crime=@_desc),
    neighborhood=(SELECT id FROM neighborhood WHERE neighborhood=@_neighborhood),
    district=(SELECT district FROM neighborhood WHERE neighborhood=@_neighborhood),
    latitude=SUBSTRING(SUBSTRING_INDEX(@_location1,',',1),2),
    longitude=SUBSTRING_INDEX(SUBSTRING_INDEX(@_location1,',',-1),')',1);

DROP DATABASE IF EXISTS crime;
CREATE DATABASE crime;

use crime;

CREATE TABLE crimeType (
    id              int(2)  PRIMARY KEY auto_increment,
    crime           varchar(20),
    violent         int(1) COMMENT '0 for non-violent 1 for violent'
);

CREATE TABLE district (
    id              int(1)  PRIMARY KEY auto_increment,
    district        varchar(12)
);

CREATE TABLE neighborhood (
    id              int     PRIMARY KEY auto_increment,
    neighborhood    varchar(33),
    population      int,
    white           int,
    african_american int,
    native_american int,
    asian           int,
    pacific_islander int,
    other_race      int,
    two_or_more_race int,
    hispanic        int,
    male            int,
    female          int,
    age_0_4         int,
    age_5_11        int,
    age_12_14       int,
    age_15_17       int,
    age_18_24       int,
    age_24_34       int,
    age_35_44       int,
    age_45_64       int,
    age_over_65     int,
    families        int,
    married         int,
    married_18      int,
    male_head_household int,
    male_head_household_18 int,
    female_head_household int, 
    female_head_household_18 int,
    housing         int,
    occupied        int,
    occupied_by_owner int,
    occupied_by_renter int,
    vacant          int,
    vacant_rent     int,
    vacant_sale     int,
    vacant_other    int,
    district        int(1),
    area            double,
    c_lat           double,
    c_lon           double,
    FOREIGN KEY (district) REFERENCES district(id)
);

CREATE TABLE victimReport (
    id              int     PRIMARY KEY auto_increment,
    date            DATE,
    time            TIME,
    crimeCode       char(3),
    location        varchar(20),
    crime           int(2),
    weapon          varchar(16),
    post            char(3),
    district        int(1),
    neighborhood    int,
    latitude        double,
    longitude       double,
    FOREIGN KEY (crime) REFERENCES crimeType(id),
    FOREIGN KEY (district) REFERENCES district(id),
    FOREIGN KEY (neighborhood) REFERENCES neighborhood(id)
);

#!/bin/bash

# Since it's a bad idea to store passwords in version control,
# keep a local, non-versioned dbConfig file on each server
source ./dbConfig

mysql -u $MS_USR --password=$MS_PASS < crime-ddl.sql || { echo "$(date) ERROR: Failed to load DDL" >> log; exit 1; }
echo "$(date) INFO: Loaded DDL" >> log
mysql -u $MS_USR --password=$MS_PASS < crimeType.sql || { echo "$(date) ERROR: Failed to load crimeType data" >> log; exit 1; }
echo "$(date) INFO: Loaded crimeType data" >> log
mysql -u $MS_USR --password=$MS_PASS < district.sql || { echo "$(date) ERROR: Failed to load district data" >> log; exit 1; }
echo "$(date) INFO: Loaded district data" >> log
mysql --local-infile -u $MS_USR --password=$MS_PASS $MS_DB < loadCrime.sql || { echo "$(date) ERROR: Failed to load victim data" >> log; exit 1; } 
echo "$(date) INFO: Loaded victim data" >> log

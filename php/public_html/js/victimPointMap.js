function victimPointMap(canvas, settings) {

    var projection = settings.projection;
    var dataUri = "http://" + window.location.hostname + 
        ":8080/crime/victimService/summary/crimelocation";

    // what portion of data to draw upon
    if (typeof(settings.district) !== 'undefined')
        dataUri += "?district=" + settings.district;
    else if (typeof(settings.neighborhood) !== 'undefined')
        dataUri += "?neighborhood=" + settings.neighborhood;

    d3.json(dataUri, function(error, victimPoints) {
        if (error) throw error;

        // how opaque to make the color
        var oScale = d3.scale.linear()
            .domain([
                1,
                d3.max(victimPoints, function(d) { return d.totalCrimes; })
            ])
            .range([0.1,1]);

        var cScale = d3.scale.linear()
            .domain([
                0,
                d3.max(victimPoints, function(d) { return d.violentCrimes / d.totalCrimes; })
            ])
            .range([0,120]);
                
        if (settings.drawType == "canvas") {     
            // draw circles for each point
            var context = canvas.getContext('2d');
            
            victimPoints.forEach(function(d) {
                var coords = projection([d.longitude, d.latitude]);
                context.beginPath();
                context.arc(coords[0], coords[1], 2, 2 * Math.PI, false);
                context.fillStyle = "hsla(" + cScale(d.violentCrimes/d.totalCrimes) + ",100%,50%," + oScale(d.totalCrimes) + ")";
                context.fill();
            });
        }
        else {
            var circle = canvas.selectAll("circle")
                .data(victimPoints);
                
            circle.enter()
                .append("circle")
                    .attr("r", 2)
                    .attr("cx", function(d) { return projection([d.longitude, d.latitude])[0]; })
                    .attr("cy", function(d) { return projection([d.longitude, d.latitude])[1]; })
                    .attr("fill", function(d) { return "hsla(" + cScale(d.violentCrimes/d.totalCrimes) + ",100%,50%," + oScale(d.totalCrimes) + ")"; });
        }

    });
}

function districtMap(svg, settings) {

    var path = d3.geo.path().projection(settings.projection);

    var districts = [
        "Central",
        "Eastern",
        "Northeastern",
        "Northern",
        "Northwestern",
        "Southeastern",
        "Southern",
        "Southwestern",
        "Western"
        ];

    if (typeof(settings.neighborhood) === 'undefined' && typeof(settings.district) === 'undefined') {
        svg.selectAll(".poly-district")
            .data(districts)
            .enter()
            .append("path")
                .attr("class", function(d) { return "poly-district " + d; })
                .datum(function(dist) {
                    return topojson.merge(settings.mapData, settings.mapData.objects["neighborhoods.geo"].geometries.filter(
                        function(d) { return dist === d.properties.district; }
                    ));
                })
                .attr("d", path);
    }
    else if (settings.district !== 'undefined') {
        svg.selectAll(".ploy-district")
            .data([settings.district])
            .enter()
            .append("path")
                .attr("class", function(d) { return "poly-district " + d; })
                .datum(function(dist) {
                    return topojson.merge(settings.mapData, settings.mapData.objects["neighborhoods.geo"].geometries.filter(
                        function(d) { return dist === d.properties.district; }
                    ));
                })
                .attr("d", path);
    }
}


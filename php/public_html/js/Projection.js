function Projection(feature, width, height) {
    var projection = d3.geo.mercator();
    
    var bounds = d3.geo.bounds(feature);
    var featWidth = bounds[1][0] - bounds[0][0];
    var featHeight = bounds[1][1] - bounds[0][1];
    var featCenter = [
        (bounds[0][0] + bounds[1][0]) / 2,
        (bounds[0][1] + bounds[1][1]) / 2
    ];

    // center the projection on the center of the feature bounds
    projection.center(featCenter);
    projection.translate([width/2,height/2 - 75])
    
    if (featWidth > featHeight) {
        // scale to height of feature
        projection.scale( 35 * width / featWidth);
    }
    else {
        projection.scale( 40 * height / featHeight );
    }

    console.log(projection.scale(), projection.translate());
    return projection;
}

/**
* Handles projections of the individual city police districts.
*/
function DistrictProjection(feature, district) {
console.log(district);
    var distProjData = {
        "Central": {
            scale: 500000,
            trans: [227,250]
        },
        "Eastern": {
            scale: 320000,
            trans: [200,250]
        },
        "Northeastern": {
            scale: 300000,
            trans: [227,300]
        },
        "Northern": {
            scale: 280000,
            trans: [227,275]
        },
        "Northwestern": {
            scale: 400000,
            trans: [227,300]
        },
        "Southeastern": {
            scale: 300000,
            trans: [227,300]
        },
        "Southern": {
            scale: 200000,
            trans: [227,300]
        },
        "Southwestern": {
            scale: 300000,
            trans: [227,250]
        },
        "Western": {
            scale: 500000,
            trans: [227,250]
        }
    }
    var projection = d3.geo.mercator();
    
    var bounds = d3.geo.bounds(feature);
    var featWidth = bounds[1][0] - bounds[0][0];
    var featHeight = bounds[1][1] - bounds[0][1];
    var featCenter = [
        (bounds[0][0] + bounds[1][0]) / 2,
        (bounds[0][1] + bounds[1][1]) / 2
    ];

    // center the projection on the center of the feature bounds
    projection.center(featCenter);
    projection.translate(distProjData[district].trans);
    projection.scale(distProjData[district].scale);

    return projection;
}

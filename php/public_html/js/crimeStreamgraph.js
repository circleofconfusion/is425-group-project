function crimeStreamgraph() {
    // The value for "this" is the div that contains the streamgraph
    var container = this;
    var bbox = container.node().getBoundingClientRect();
    var margin = { top:10, left:50, right:10, bottom:20 };
    var location = this.data()[0].location;
    var crimeData = this.data()[0].crimeData;
    var periodType = crimeData[0].periodType;

    // figure out how to parse the date
    if (periodType == 'month') {
        crimeData.forEach(function(pd) {
            pd.periodData.forEach(function(d) {
                d.date = new Date(d.year, (d.periodNumber - 1));
            });
        });
    }
    else if (periodType == 'week') {
        crimeData.forEach(function(pd) {
            pd.periodData.forEach(function(d) {
                d.date = new Date(d.year, 0, (d.periodNumberi * 7));
            });
        });
    }
    else if (periodType == 'day') {
        crimeData.forEach(function(pd) {
            pd.periodData.forEach(function(d) {
                d.date = new Date(d.year, 0, d.periodNumberi);
            });
        });
    }


    // create the actual SVG element 
    var svg = container.append("svg")
        .attr("width", bbox.width)
        .attr("height", bbox.height)
        .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // plot scaling functions
    var x = d3.time.scale()
        .range([0, bbox.width - margin.left - margin.right])
        .domain([
            d3.min(crimeData, function(d) { 
                return d3.min(d.periodData, function(d) { 
                    return d.date; 
                }); 
            }), 
            d3.max(crimeData, function(d) {
                return d3.max(d.periodData, function(d) {
                    return d.date;
                });
            })
        ]);

    var y = d3.scale.linear()
        .range([5, bbox.height - margin.top - margin.bottom - 1])

    // set up the stacking function
    var stack = d3.layout.stack()
        .offset("silhouette")
        .values(function(d) { return d.periodData; })
        .x(function(d) { return d.date; })
        .y(function(d) { return d.numCrimes; });

    // set up the area function 
    var area = d3.svg.area()
        .interpolate("basis")
        .x(function(d) { return x(d.date); })
        .y0(function(d) { return y(d.y0); })
        .y1(function(d) { return y(d.y0 + d.y); });

    var layers = stack(crimeData);

    y.domain([
        d3.max(crimeData, function(d) { 
            return d3.max(d.periodData, function(d) {
                return d.y + d.y0; 
            }); 
        }),
        0 
    ]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var dividedY = d3.scale.linear()
        .domain(y.domain())
        .range([5, bbox.height - margin.bottom - 5])

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    svg.selectAll(".layer-crime")
        .data(layers)
        .enter()
        .append("path")
            .attr("class", function(d) { 
                return "layer-crime " + d.crime.toLowerCase().replace(" - ", "-").replace(/ /g,"-"); })
            .attr("d", function(d) { return area(d.periodData); })
            .attr("title", function(d) { return d.crime; });

    svg.append("g")
        .attr("class","x axis")
        .attr("transform", "translate(0, " + (bbox.height - margin.top - margin.bottom) + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);
}

function crimeAreaPlot() {
    // The value for "this" is the div that contains the streamgraph
    var container = this;
    var bbox = container.node().getBoundingClientRect();
    var margin = { top:10, left:50, right:20, bottom:20 };
    var location = this.data()[0].location;
    var crimeData = this.data()[0].crimeData;
    var periodType = crimeData[0].periodType;

    // figure out how to parse the date
    if (periodType == 'month') {
        crimeData.forEach(function(pd) {
            pd.periodData.forEach(function(d) {
                d.date = new Date(d.year, (d.periodNumber - 1));
            });
        });
    }
    else if (periodType == 'week') {
        crimeData.forEach(function(pd) {
            pd.periodData.forEach(function(d) {
                d.date = new Date(d.year, 0, (d.periodNumberi * 7));
            });
        });
    }
    else if (periodType == 'day') {
        crimeData.forEach(function(pd) {
            pd.periodData.forEach(function(d) {
                d.date = new Date(d.year, 0, d.periodNumberi);
            });
        });
    }


    // create the actual SVG element 
    var svg = container.append("svg")
        .attr("width", bbox.width)
        .attr("height", bbox.height)
        .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // plot scaling functions
    var x = d3.time.scale()
        .range([0, bbox.width - margin.left - margin.right])
        .domain([
            d3.min(crimeData, function(d) { 
                return d3.min(d.periodData, function(d) { 
                    return d.date; 
                }); 
            }), 
            d3.max(crimeData, function(d) {
                return d3.max(d.periodData, function(d) {
                    return d.date;
                });
            })
        ]);

    var y = d3.scale.linear()
        .range([0, bbox.height - margin.top - margin.bottom - 1])

    // set up the stacking function
    var stack = d3.layout.stack()
        .offset("zero")
        .values(function(d) { return d.periodData; })
        .x(function(d) { return d.date; })
        .y(function(d) { return d.numCrimes; });

    // set up the area function 
    var area = d3.svg.area()
        .interpolate("basis")
        .x(function(d) { return x(d.date); })
        .y0(function(d) { return y(d.y0); })
        .y1(function(d) { return y(d.y0 + d.y); });

    var layers = stack(crimeData);

    y.domain([
        d3.max(crimeData, function(d) { 
            return d3.max(d.periodData, function(d) {
                return d.y + d.y0; 
            }); 
        }),
        0 
    ]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    svg.selectAll(".layer-crime")
        .data(layers)
        .enter()
        .append("path")
            .attr("class", function(d) { 
                return "layer-crime " + d.crime.toLowerCase().replace(" - ", "-").replace(/ /g,"-"); })
            .attr("d", function(d) { return area(d.periodData); })
            .attr("title", function(d) { return d.crime; });

    svg.append("g")
        .attr("class","x axis")
        .attr("transform", "translate(0, " + (bbox.height - margin.top - margin.bottom) + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "x axis")
        .call(yAxis);
}


function eachCrimeStreamgraph() {
    var sgContainer = d3.select("#portfolioModal3 #streamgraph-container");
    // clear out any lingering plots
    sgContainer.selectAll("div").remove();

    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/summary/city/month/", function(error, data) {
        if (error) throw error;

        var sgLocation = sgContainer.selectAll(".streamgraph-location-city")
            .data(data)
            .enter()
            .append("div")
                .attr("class", "streamgraph-location-city");

        var sgLocationLabel = sgLocation.append("div")
            .attr("class", "streamgraph-label")
            .append("span")
                .text(function(d) { return d.location; });

        var sgLocationPlot = sgLocation.append("div")
            .attr("class", "streamgraph-plot");
    
        sgLocationPlot[0].forEach(function(div) {
            d3.select(div).call(crimeStreamgraph);
        });

    });

    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/summary/district/month/", function(error, data) {
        if (error) throw error;

        var sgLocation = sgContainer.selectAll(".streamgraph-location")
            .data(data)
            .enter()
            .append("div")
                .attr("class", "streamgraph-location");

        var sgLocationLabel = sgLocation.append("div")
            .attr("class", "streamgraph-label")
            .append("span")
                .text(function(d) { return d.location; });

        var sgLocationPlot = sgLocation.append("div")
            .attr("class", "streamgraph-plot");
    
        sgLocationPlot[0].forEach(function(div) {
            d3.select(div).call(crimeStreamgraph);
        });
    });
}

function violentNonviolentStreamgraph() {
    var sgContainer = d3.select("#portfolioModal3 #streamgraph-container");
    // clear out any lingering plots
    sgContainer.selectAll("div").remove();

    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/summary/violent-nonviolent/city/month/both", function(error, data) {
        if (error) throw error;

        var sgLocation = sgContainer.selectAll(".streamgraph-location-city")
            .data(data)
            .enter()
            .append("div")
                .attr("class", "streamgraph-location-city");

        var sgLocationLabel = sgLocation.append("div")
            .attr("class", "streamgraph-label")
            .append("span")
                .text(function(d) { return d.location; });

        var sgLocationPlot = sgLocation.append("div")
            .attr("class", "streamgraph-plot");
    
        sgLocationPlot[0].forEach(function(div) {
            d3.select(div).call(crimeStreamgraph);
        });

    });

    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/summary/violent-nonviolent/district/month/both", function(error, data) {
        if (error) throw error;

        var sgLocation = sgContainer.selectAll(".streamgraph-location")
            .data(data)
            .enter()
            .append("div")
                .attr("class", "streamgraph-location");

        var sgLocationLabel = sgLocation.append("div")
            .attr("class", "streamgraph-label")
            .append("span")
                .text(function(d) { return d.location; });

        var sgLocationPlot = sgLocation.append("div")
            .attr("class", "streamgraph-plot");
    
        sgLocationPlot[0].forEach(function(div) {
            d3.select(div).call(crimeStreamgraph);
        });
    });
}

function violentAreaPlot(crimeType) {
    var sgContainer = d3.select("#portfolioModal3 #streamgraph-container");
    // clear out any lingering plots
    sgContainer.selectAll("div").remove();

    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/summary/violent-nonviolent/city/month/" + crimeType, function(error, data) {
        if (error) throw error;

        var sgLocation = sgContainer.selectAll(".streamgraph-location-city")
            .data(data)
            .enter()
            .append("div")
                .attr("class", "streamgraph-location-city");

        var sgLocationLabel = sgLocation.append("div")
            .attr("class", "streamgraph-label")
            .append("span")
                .text(function(d) { return d.location; });

        var sgLocationPlot = sgLocation.append("div")
            .attr("class", "streamgraph-plot");
    
        sgLocationPlot[0].forEach(function(div) {
            d3.select(div).call(crimeAreaPlot);
        });

    });

    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/summary/violent-nonviolent/district/month/" + crimeType, function(error, data) {
        if (error) throw error;

        var sgLocation = sgContainer.selectAll(".streamgraph-location")
            .data(data)
            .enter()
            .append("div")
                .attr("class", "streamgraph-location");

        var sgLocationLabel = sgLocation.append("div")
            .attr("class", "streamgraph-label")
            .append("span")
                .text(function(d) { return d.location; });

        var sgLocationPlot = sgLocation.append("div")
            .attr("class", "streamgraph-plot");
    
        sgLocationPlot[0].forEach(function(div) {
            d3.select(div).call(crimeAreaPlot);
        });
    });
}

function crimeTypeAreaPlot(crimeType) {
    var sgContainer = d3.select("#portfolioModal3 #streamgraph-container");
    // clear out any lingering plots
    sgContainer.selectAll("div").remove();

    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/summary/city/month?crimeType=" + crimeType, function(error, data) {
        if (error) throw error;

        var sgLocation = sgContainer.selectAll(".streamgraph-location-city")
            .data(data)
            .enter()
            .append("div")
                .attr("class", "streamgraph-location-city");

        var sgLocationLabel = sgLocation.append("div")
            .attr("class", "streamgraph-label")
            .append("span")
                .text(function(d) { return d.location; });

        var sgLocationPlot = sgLocation.append("div")
            .attr("class", "streamgraph-plot");
    
        sgLocationPlot[0].forEach(function(div) {
            d3.select(div).call(crimeAreaPlot);
        });

    });

    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/summary/district/month?crimeType=" + crimeType, function(error, data) {
        if (error) throw error;

        var sgLocation = sgContainer.selectAll(".streamgraph-location")
            .data(data)
            .enter()
            .append("div")
                .attr("class", "streamgraph-location");

        var sgLocationLabel = sgLocation.append("div")
            .attr("class", "streamgraph-label")
            .append("span")
                .text(function(d) { return d.location; });

        var sgLocationPlot = sgLocation.append("div")
            .attr("class", "streamgraph-plot");
    
        sgLocationPlot[0].forEach(function(div) {
            d3.select(div).call(crimeAreaPlot);
        });
    });
}

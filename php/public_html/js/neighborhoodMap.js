function neighborhoodMap(svg, settings) {
    var path = d3.geo.path().projection(settings.projection);
    var features;
    
    if (typeof(settings.district) !== 'undefined') {
        features = topojson.feature(settings.mapData, settings.mapData.objects["neighborhoods.geo"]).features.filter(function(d) {
            return d.properties.district === settings.district;
        });
    }
    else {
        features = topojson.feature(settings.mapData, settings.mapData.objects["neighborhoods.geo"]).features;
    }

    svg.append("g")
        .attr("class","neighborhoods-group")
        .selectAll(".poly-neighborhood")
            .data(features)
            .enter()
            .append("path")
                .attr("class", function(d) { return "poly-neighborhood " + d.properties.district; })
                .attr("d", path)
                .style("stroke","white")
                .attr("id", function(d) { return d.properties.name; })
                .attr("title", function(d) { return d.properties.name; });

}

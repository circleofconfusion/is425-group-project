/*************************************************************************
 * @file map.js
 * @author Shane Knudsen
 *
 * Various map functions for drawing maps of Baltimore's police districts
 * and neighborhoods.
 ************************************************************************/

/**
 * Draws neighborhood outlines.
 */
function neighborhoodMap(svg, settings) {
    var path = d3.geo.path().projection(settings.projection);
    var features;
    
    if (typeof(settings.district) !== 'undefined') {
        features = topojson.feature(settings.mapData, settings.mapData.objects["neighborhoods.geo"]).features.filter(function(d) {
            return d.properties.district === settings.district;
        });
    }
    else {
        features = topojson.feature(settings.mapData, settings.mapData.objects["neighborhoods.geo"]).features;
    }

    svg.append("g")
        .attr("class","neighborhoods-group")
        .selectAll(".poly-neighborhood")
            .data(features)
            .enter()
            .append("path")
                .attr("class", function(d) { return "poly-neighborhood " + d.properties.district; })
                .attr("d", path)
                .style("stroke","white")
                .attr("id", function(d) { return d.properties.name; })
                .attr("title", function(d) { return d.properties.name; });

}

/**
 * Draws all or some of the police districts in Baltimore.
 */
function districtMap(svg, settings) {

    var path = d3.geo.path().projection(settings.projection);

    var districts = [
        "Central",
        "Eastern",
        "Northeastern",
        "Northern",
        "Northwestern",
        "Southeastern",
        "Southern",
        "Southwestern",
        "Western"
        ];

    var selector = ".poly-district";
    var className = "poly-district";
    if (settings.border === true) {
        selector += ".border";
        className += " border";
    }
    else if (settings.border === false) {
        selector += ".noborder";
        className += " noborder";
    }
    if (settings.fill === true) {
        selector += ".fill";
        className += " fill";
    }
    else if (settings.fill === false) {
        selector += ".nofill";
        className += " nofill";
    }
    if (settings.transparent === true) {
        selector += ".transparent";
        className += " transparent";
    }
   
    // No district has been specified, draw all districts.
    if (typeof(settings.district) === 'undefined') {
        svg.selectAll(selector)
            .data(districts)
            .enter()
            .append("path")
                .attr("class", function(d) { return className + " " + d; })
                .attr("data-district", function(d) { return d; })
                .datum(function(dist) {
                    return topojson.merge(settings.mapData, settings.mapData.objects["neighborhoods.geo"].geometries.filter(
                        function(d) { return dist === d.properties.district; }
                    ));
                })
                .attr("d", path);

    }
    // Otherwise, grab the specified district, and draw it.
    else {
        svg.selectAll(selector)
            .data([settings.district])
            .enter()
            .append("path")
                .attr("class", function(d) { return className + " " + d; })
                .attr("data-district", function(d) { return d; })
                .datum(function(dist) {
                    return topojson.merge(settings.mapData, settings.mapData.objects["neighborhoods.geo"].geometries.filter(
                        function(d) { return dist === d.properties.district; }
                    ));
                })
                .attr("d", path);
    }
}

/**
 * Draws the entire city and violent/nonviolent victim points.
 */
function drawCity(map, mapData) {
    // clear out any previous predictions
    d3.selectAll("#portfolioModal1 .prediction span").remove();

    // set the prediction
    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/periodPrediction/city",
        function(error, data) {
        
        if (error) throw error;

        var predictionDiv = d3.select("#portfolioModal1 .prediction");
        data.forEach(function(d) {
            predictionDiv.append("span")
                .attr("class", "prediction-number")
                .text(d.expectedMin + "-" + d.expectedMax);

            predictionDiv.append("span")
                .text(" crimes expected this " + d.periodType);
        });
    });

    // set the breadcrumb Trail
    var bc = d3.select("#portfolioModal1 .breadcrumb");
    bc.html("");
    bc.append("span").attr("class", "city").text("Baltimore");

    clearMap(map);
    
    d3.select("#map-live").style("flex", "100% 1 0");
    d3.select("#detail-area").style("flex", "0% 1 0");
    var WIDTH = map.node().clientWidth;
    var HEIGHT = map.node().clientHeight;
    
    // set city title
    d3.select("#map-locale").text("Baltimore City");

    // set the projection for this map
    var projection = Projection(
        topojson.merge(mapData, mapData.objects["neighborhoods.geo"].geometries.filter(function() {return true;})), 
        WIDTH, HEIGHT);
    
    var svgBack = map.append("svg")
        .attr("class", "background")
        .attr("width", WIDTH)
        .attr("height", HEIGHT);

    var canvas = map.append("canvas")
        .attr("id", "point-victim")
        .attr("width", WIDTH)
        .attr("height", HEIGHT);
        
    var svgFore = map.append("svg")
        .attr("class", "foreground")
        .attr("width", WIDTH)
        .attr("height", HEIGHT);
    
    // draw the district background colors
    svgBack.call(districtMap, {
        projection: projection,
        mapData: mapData,
        border: false,
        fill: true
    });
    // draw the neighborhoods
    svgBack.call(neighborhoodMap, {
        projection: projection,
        mapData: mapData
    });
    // draw the district borders
    svgBack.call(districtMap, {
        projection: projection,
        mapData: mapData,
        border: true,
        fill: false
    });

    // draw the district borders
    svgFore.call(districtMap, {
        projection: projection,
        mapData: mapData,
        border: false,
        transparent: true
    });

    victimPointMap(document.getElementById('point-victim'), {
        projection: projection,
        drawType: "canvas"
    });
    
    svgFore.selectAll("path.poly-district").on("click",function() {
        var elemClicked = d3.select(this);
        var district = elemClicked.attr("data-district");
        drawDistrict(map, mapData, district);
    }); 
}

/**
 * Draws just the specified district
 */
function drawDistrict(map, mapData, district) {
    // clear out any previous predictions
    d3.selectAll("#portfolioModal1 .prediction span").remove();

    // set the prediction
    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/periodPrediction/district?name=" + district,
        function(error, data) {
        
        if (error) throw error;

        var predictionDiv = d3.select("#portfolioModal1 .prediction");
        data.forEach(function(d) {
            predictionDiv.append("span")
                .attr("class", "prediction-number")
                .text(d.expectedMin + "-" + d.expectedMax);

            predictionDiv.append("span")
                .text(" crimes expected this " + d.periodType);
        });
    });

    // set the breadcrumb Trail
    var bc = d3.select("#portfolioModal1 .breadcrumb");
    bc.html("");
    bc.append("span").attr("class", "bc-city pointer").text("Baltimore");
    bc.append("span").attr("class", "bc-divider").text(" » ");
    bc.append("span").attr("class", "bc-district").text(district + " District");
    bc.select(".bc-city").on("click", function() { drawCity(map, mapData); });
    
    clearMap(map);
    
    d3.select("#map-live").style("flex", "48% 1 0");
    d3.select("#detail-area").style("flex", "48% 1 0");
    
    var WIDTH = map.node().clientWidth;
    var HEIGHT = map.node().clientHeight;
    
    // set district title
    d3.select("#map-locale").text(district + " District");

    // set the projection for this map
 /*   var projection = Projection(
        topojson.merge(mapData, mapData.objects["neighborhoods.geo"].geometries.filter(function(d) {
            return d.properties.district == district; 
        })), 
        WIDTH, HEIGHT);*/

    var projection = DistrictProjection(topojson.merge(mapData, mapData.objects["neighborhoods.geo"].geometries.filter(function(d) {
            return d.properties.district == district; 
        })), district); 

    var svg = map.append("svg")
        .attr("class", "background")
        .attr("width", WIDTH)
        .attr("height", HEIGHT);

    svg.call(districtMap, {
        projection: projection,
        mapData: mapData,
        district: district,
        border: false,
        fill: true
    });

    svg.call(neighborhoodMap, {
        projection: projection,
        mapData: mapData,
        district: district
    });

    svg.call(districtMap, {
        projection: projection,
        mapData: mapData,
        district: district,
        border: true,
        fill: false
    });

    victimPointMap(svg, {
        projection: projection,
        drawType: "svg",
        district: district
    });
}

function victimPointMap(canvas, settings) {

    var projection = settings.projection;
    var dataUri = "http://" + window.location.hostname + 
        ":8080/crime/victimService/summary/crimelocation";

    // what portion of data to draw upon
    if (typeof(settings.district) !== 'undefined')
        dataUri += "?district=" + settings.district;
    else if (typeof(settings.neighborhood) !== 'undefined')
        dataUri += "?neighborhood=" + settings.neighborhood;

    d3.json(dataUri, function(error, victimPoints) {
        if (error) throw error;

        // how opaque to make the color
        var oScale = d3.scale.linear()
            .domain([
                1,
                d3.max(victimPoints, function(d) { return d.totalCrimes; })
            ])
            .range([0.1,1]);

        var cScale = d3.scale.linear()
            .domain([
                0,
                d3.max(victimPoints, function(d) { return d.violentCrimes / d.totalCrimes; })
            ])
            .range([0,120]);
                
        if (settings.drawType == "canvas") {     
            // draw circles for each point
            var context = canvas.getContext('2d');
            
            victimPoints.forEach(function(d) {
                var coords = projection([d.longitude, d.latitude]);
                context.beginPath();
                context.arc(coords[0], coords[1], 2, 2 * Math.PI, false);
                context.fillStyle = "hsla(" + cScale(d.violentCrimes/d.totalCrimes) + ",100%,50%," + oScale(d.totalCrimes) + ")";
                context.fill();
            });
        }
        else {
            var circle = canvas.selectAll("circle")
                .data(victimPoints);
                
            circle.enter()
                .append("circle")
                    .attr("r", 2)
                    .attr("cx", function(d) { return projection([d.longitude, d.latitude])[0]; })
                    .attr("cy", function(d) { return projection([d.longitude, d.latitude])[1]; })
                    .attr("fill", function(d) { return "hsla(" + cScale(d.violentCrimes/d.totalCrimes) + ",100%,50%," + oScale(d.totalCrimes) + ")"; })
                    .on('click', function(d) { victimDetail(d) });
        }

    });
}

/**
 * Displays crime records when a victim circle is clicked.
 */
function victimDetail(point) {

    // clear out the old table and heading
    d3.select(".detail-table-heading").remove();
    d3.select(".detail-table").remove();
    
    // gather data from the REST endpoint
    d3.json("http://" + window.location.hostname + ":8080/crime/victimService/crimelocation/detail?latitude=" + 
        point.latitude + "&longitude=" + point.longitude,
        function(error, data) {
            if (error) throw error;
 
            // add the section heading
            d3.select("#detail-area")
                .append("h3")
                .attr("class", "detail-table-heading")
                .text(getAddress() + getNeighborhood());

            // add a new table
            var table = d3.select("#detail-area")
                .append("table")
                    .attr("class", "detail-table");

            // add heading
            var thead = table.append("thead").append("tr");
            thead.append("th").text("Crime");
            thead.append("th").text("Weapon");
            thead.append("th").text("Date");
            thead.append("th").text("Time");

            // add the table body
            var tbody = table.append("tbody");

            // for each returned object, create a table row
            data.forEach(function(d) {
                var row = tbody.append("tr");
                row.append("td").text(allTitleCase(d.crime));
                row.append("td").text(allTitleCase(d.weapon));
                row.append("td").text(d.date);
                row.append("td").text(d.time);
            });

            /**
             * Gets the street address of the coordinates, if any.
             */
            function getAddress() {
                var address = "";
                for (var i = 0; i < data.length; ++i) { 
                    address = data[i].location;
                    if (address != "")
                        break;
                }

                return address + " ";
            }

            /**
             * Gets the neighborhood of the coordinates, if any.
             */
            function getNeighborhood() {
                var neighborhood = "";
                for (var i = 0; i < data.length; ++i) { 
                    neighborhood = data[i].neighborhood;
                    if (neighborhood != "")
                        break;
                }

                return neighborhood;
            }
        });
}

/**
 * Delete any old svg/canvas elements that may be lingering.
 */
function clearMap(map) {
    map.selectAll("canvas").remove();
    map.selectAll("svg").remove();
}

/**
 * Capitalizes each word in a string.
 */
function allTitleCase(inStr) { 
    return inStr.replace(/\w\S*/g, function(tStr) { 
        return tStr.charAt(0).toUpperCase() + tStr.substr(1).toLowerCase(); 
    });
}

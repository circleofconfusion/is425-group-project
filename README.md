# IS 425 Group Project

Shane Knudsen

Emelia Berg

Chidi Okunji

Eric Lee

## Contents

### java/

Holds all the Java/Tomcat code for the REST interface.

### map_data/

Holds all Shapefiles that are used to generate our maps. Includes a GNU makefile
to make converting them to TopoJson files easier.

### php/

Holds all the PHP code for the frontend.

### sql/

Holds all the SQL code to build the database.

To use these files, enter the following at a BASH prompt from the sql directory:

    ./resetdb.sh

## Notes on the Java part

### Building

Java, unlike PHP needs to be compiled first. To make things a whole lot
simpler, the Java code is set up as a 
[Maven project](https://maven.apache.org/what-is-maven.html). 
Maven gathers all the dependencies (Jersey, MYSQL driver, etc.) that the code 
needs, builds it, and sets it up as a ready-to-deploy J2EE webapp.

### Database Authentication

This code will build, but will be unable to connect to the database until a
a file named db.properties is added to java/src/webapp/WEB-INF/classes/ 
Shane can get you this file out of band.